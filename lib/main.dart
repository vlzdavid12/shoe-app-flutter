import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoepapp/models/shoe_model.dart';
import 'package:shoepapp/pages/shoep_page.dart';

void main() {
  return runApp(
   MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ShoeModel())
      ],
      child: MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Shoep App Flutter',
      home: ShoepPage()
    );
  }
}

