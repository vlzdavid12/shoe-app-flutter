import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoepapp/helpers/helpers.dart';
import 'package:shoepapp/widgets/custom_widgets.dart';

import '../models/shoe_model.dart';

class ShoepDescPage extends StatelessWidget {
  const ShoepDescPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    changeStatusLight();
    return Scaffold(
        body: Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Hero(tag: 'shoe-1', child: ShoeSizePreview(fullScreen: true)),
            Positioned(
                top: 80,
                child: FloatingActionButton(
                  onPressed: () {
                    Navigator.pop(context);
                    changeStatusDark();
                  },
                  elevation: 0,
                  highlightElevation: 0,
                  backgroundColor: Colors.transparent,
                  child:
                      Icon(Icons.chevron_left, color: Colors.white, size: 60),
                ))
          ],
        ),
        Expanded(
            child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ShoeDescription(
                  title: 'Nike Air Max 720',
                  description:
                      "The Nike Air Max 720 goes bigger than ever before with Nike's taller Air unit yet, offering more air underfoot for unimaginable, all-day comfort. Has Air Max gone too far? We hope so."),
              _MountBuyNow(),
              _ListColors(),
              _ButtonsLikeCartShared()
            ],
          ),
        ))
      ],
    ));
  }
}

class _MountBuyNow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        margin: const EdgeInsets.only(bottom: 20, top: 20),
        child: Row(
          children: <Widget>[
            Text('\$180.0',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            Spacer(),
            Bounce(
              delay: const Duration(seconds: 1),
              from: 8,
              child: ButtonOrange(
                text: 'Buy Now',
                widthContainer: 120,
                heightContainer: 40,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _ListColors extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Container(
                height: 80,
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    Positioned(left: 90, child: _BtnColor(Color(0xffC6D642), 4, 'assets/imgs/verde.png')),
                    Positioned(left: 60, child: _BtnColor(Color(0xffFFAD29), 3, 'assets/imgs/amarillo.png')),
                    Positioned(left: 30, child: _BtnColor(Color(0xff2099F1), 2, 'assets/imgs/azul.png')),
                    Positioned(left: 0, child: _BtnColor(Color(0xff364D56), 1, 'assets/imgs/negro.png')),
                  ],
                ),
              )),
          Expanded(
            child: ButtonOrange(
                text: 'More related items',
                heightContainer: 30,
                widthContainer: 130,
                colorContainer: const Color(0xc5ffad29)),
          ),
        ],
      ),
    );
  }
}

class _BtnColor extends StatelessWidget {
  final Color color;
  final int index;
  final String urlImage;

  _BtnColor(this.color, this.index, this.urlImage);

  @override
  Widget build(BuildContext context) {
    return FadeInLeft(
      delay: Duration(milliseconds:  index * 200),
      duration: Duration(milliseconds: 500),
      child: GestureDetector(
        onTap: (){
          final shoeModel =  Provider.of<ShoeModel>(context, listen: false);
          shoeModel.assetImage =  urlImage;
        },
        child: Container(
          width: 45,
          height: 45,
          decoration: BoxDecoration(color: color, shape: BoxShape.circle),
        ),
      ),
    );
  }
}

class _ButtonsLikeCartShared extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 30),
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _ButtonShadow(const Icon(Icons.start, color: Colors.red, size: 25)),
          _ButtonShadow(Icon(
            Icons.add_shopping_cart,
            color: Colors.grey.withOpacity(0.4),
            size: 25,
          )),
          _ButtonShadow(Icon(
            Icons.settings,
            color: Colors.grey.withOpacity(0.4),
            size: 25,
          ))
        ],
      ),
    );
  }
}

class _ButtonShadow extends StatelessWidget {
  final Icon icon;

  _ButtonShadow(this.icon);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 55,
      height: 55,
      decoration: const BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                spreadRadius: -5,
                blurRadius: 20,
                offset: Offset(0, 10))
          ]),
      child: icon,
    );
  }
}
