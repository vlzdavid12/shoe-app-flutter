import 'package:flutter/material.dart';
import 'package:shoepapp/helpers/helpers.dart';
import 'package:shoepapp/widgets/custom_widgets.dart';

class ShoepPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    changeStatusDark();
    return Scaffold(
        body: Column(
      children: <Widget>[
        CustomAppBar(text: 'Four Yuo'),
        SizedBox(height: 15),
        Expanded(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                Hero(tag: 'shoe-1', child: ShoeSizePreview()),
                ShoeDescription(
                    title: 'Nike Air Max 720',
                    description:
                        "The Nike Air Max 720 goes bigger than ever before with Nike's taller Air unit yet, offering more air underfoot for unimaginable, all-day comfort. Has Air Max gone too far? We hope so."),
              ],
            ),
          ),
        ),
        AddCartBottom(
          mount: 180.00,
        )
      ],
    ));
  }
}
