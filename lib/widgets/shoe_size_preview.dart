import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoepapp/models/shoe_model.dart';
import 'package:shoepapp/pages/shoep_desc_page.dart';

class ShoeSizePreview extends StatelessWidget {
  final bool fullScreen;

  ShoeSizePreview({this.fullScreen = false});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!this.fullScreen) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => ShoepDescPage()));
        }
      },
      child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: (fullScreen) ? 5 : 20, vertical: 5),
          child: Container(
            width: double.infinity,
            height: (fullScreen) ? 350 : 420,
            decoration: BoxDecoration(
                color: Color(0xffFFCF53),
                borderRadius: (fullScreen)
                    ? BorderRadius.only(
                        bottomLeft: Radius.circular(50),
                        bottomRight: Radius.circular(50),
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25))
                    : BorderRadius.circular(50)),
            child: Column(
              children: <Widget>[
                _ShoeWithShadow(),
                if (!fullScreen) _ShoeSize(fullScreen: fullScreen)
              ],
            ),
          )),
    );
  }
}

class _ShoeWithShadow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final shoeModel = Provider.of<ShoeModel>(context);
    return Padding(
      padding: const EdgeInsets.all(30),
      child: Stack(
        children: <Widget>[
          Positioned(bottom: 10, right: 0, child: _ShoeShadow()),
          Image(image: AssetImage(shoeModel.assetImage))
        ],
      ),
    );
  }
}

class _ShoeShadow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: -0.4,
      child: Container(
        width: 230,
        height: 100,
        decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(100),
            boxShadow: [BoxShadow(color: Color(0xffEAA14E), blurRadius: 40)]),
      ),
    );
  }
}

class _ShoeSize extends StatelessWidget {
  final bool fullScreen;

  _ShoeSize({required this.fullScreen});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: (fullScreen) ? 20 : 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _SizeBox(7),
          _SizeBox(7.5),
          _SizeBox(8),
          _SizeBox(8.5),
          _SizeBox(9),
          _SizeBox(9.5),
        ],
      ),
    );
  }
}

class _SizeBox extends StatelessWidget {
  final double number;

  _SizeBox(this.number);

  @override
  Widget build(BuildContext context) {
    final shoeModel = Provider.of<ShoeModel>(context);
    return GestureDetector(
      onTap: () {
        final shoeModel = Provider.of<ShoeModel>(context, listen: false);
        shoeModel.size = number;
      },
      child: Container(
        alignment: Alignment.center,
        width: 45,
        height: 45,
        decoration: BoxDecoration(
            color: (number == shoeModel.size)
                ? const Color(0xffF1A23A)
                : Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              if (number == shoeModel.size)
                const BoxShadow(
                    color: Color(0xffF1A23A),
                    blurRadius: 10,
                    offset: Offset(0, 5))
            ]),
        child: Text('${number.toString().replaceAll('.0', '')}',
            style: TextStyle(
                color: (number == shoeModel.size)
                    ? Colors.white
                    : const Color(0xffF1A23A),
                fontSize: 16,
                fontWeight: FontWeight.bold)),
      ),
    );
  }
}
