import 'package:flutter/material.dart';
import 'package:shoepapp/widgets/custom_widgets.dart';

class AddCartBottom extends StatelessWidget {
  final double mount;
  AddCartBottom({required this.mount});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        width: double.infinity,
        height: 80,
        decoration:  BoxDecoration(
          color: Colors.grey.withOpacity(0.12),
          borderRadius: BorderRadius.circular(100)
        ),
        child: Row(
          children: <Widget>[
            const SizedBox(width: 20),
            Text('\$$mount', style: const TextStyle(fontSize: 28, fontWeight: FontWeight.w600)),
            const Spacer(),
            ButtonOrange(text: 'Add To Cart'),
            const SizedBox(width: 20),
          ],
        ),
      ),
    );
  }
}
