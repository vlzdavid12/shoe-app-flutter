import 'package:flutter/material.dart';

class ButtonOrange extends StatelessWidget {

  final String text;
  final double widthContainer;
  final double heightContainer;
  final Color colorContainer;

  ButtonOrange({required this.text, this.widthContainer = 150, this.heightContainer = 50, this.colorContainer =  Colors.orange});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: widthContainer,
      height: heightContainer,
      decoration: BoxDecoration(
        color: colorContainer,
        borderRadius: BorderRadius.circular(100)
      ),
      child: Text(text, style: const TextStyle(color: Colors.white)),
    );
  }
}
